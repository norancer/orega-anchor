# config app
app = angular.module('mainApp', [
  'ui.bootstrap',
  'ngResource',
  'ngRoute',
  'mk.editablespan',
  'ui.sortable'
])

# csrf token
app.config ($httpProvider) ->
  authToken = $("meta[name=\"csrf-token\"]").attr("content")
  $httpProvider.defaults.headers.common["X-CSRF-TOKEN"] = authToken

# config routes
app.config ($routeProvider, $locationProvider) ->
  $locationProvider.html5Mode true

# turbolinks support
$(document).on 'page:load', ->
  $('[ng-app]').each ->
    module = $(this).attr('ng-app')
    angular.bootstrap(this, [module])
